<?php
namespace app\models; // подключаем пространство имён
use yii\db\ActiveRecord; // подключаем класс ActiveRecord

class Models extends ActiveRecord // расширяем класс ActiveRecord
{
    public $model;
    public $complect;
    public $email;

    public function rules()
    {
        return [
                      // атрибут email указывает, что в переменной email должен быть корректный адрес электронной почты
            ['email', 'email'],
        ];
    }

    public function getModelscomplects()
    {
        return $this->hasMany(Models_complects::className(), ['idmodels' => 'id']);
    }



}