<?php
namespace app\models; // подключаем пространство имён
use yii\db\ActiveRecord; // подключаем класс ActiveRecord

class Models_complects extends ActiveRecord // расширяем класс ActiveRecord
{

    public function getModels()
    {
        return $this->hasOne(Models::className(), ['id' => 'idmodels']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplectations()
    {
        return $this->hasOne(Complectations::className(), ['id' => 'idcomplectations']);
    }


    public function getComplect()
    {
        return $this->hasOne(Idcomplect::className(), ['id' => 'idcomplect']);
    }



}