<?php
namespace app\models; // подключаем пространство имён
use yii\db\ActiveRecord; // подключаем класс ActiveRecord

class  Idcomplect extends ActiveRecord // расширяем класс ActiveRecord
{

    public function getModelscomplects()
    {
        return $this->hasMany(Models_complects::className(), ['idcomplect' => 'id']);
    }

}