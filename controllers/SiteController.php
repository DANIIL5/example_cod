<?php

namespace app\controllers;

use app\models\Complectations;
use app\models\Idcomplect;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Models_complects;
use app\models\Models;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionAjax()

    {
        $modelid = Yii::$app->request->get('modelid');
        $complectationid = Yii::$app->request->get('complect');
        $modelname = new Models();
        $complectname = new Complectations();

        $namemodel = $modelname::find()->where(['id' => $modelid])->asArray()->all();
        $namecomplect = $complectname::find()->where(['id' => $complectationid])->asArray()->all();

        if(isset($modelid) && !isset($complectationid)){

            $complectation=Models::findOne($modelid);
            foreach ($complectation->modelscomplects as $complects){

                $complectations[$complects->complectations->id]=$complects->complectations->title;
            }
           echo json_encode($complectations);
        }

        if(isset($modelid) && isset($complectationid)){
            $complectation=Models::findOne($modelid);
            foreach ($complectation->modelscomplects as $complects){

                if($complects->idcomplectations==$complectationid){
                    $idcomplect[$complects->idcomplect]=$complects->idcomplect;
                }



            }

            if(isset($idcomplect)){

                $technical=Idcomplect::find()->where(['id'=>$idcomplect])->asArray()->all();

                $session = Yii::$app->session;
                $session->open();
                $_SESSION['complect'] = '';
                foreach ($technical as $decription) {



                    echo '<h3>Идентификтор комплектации:</h3>';
                    echo $decription['idcomplectname'];
                    echo '</br>';
                    echo '<h3>Технические характеристики комплектации:</h3>';
                    echo $decription['description'];
                    echo '</br>';


                    $_SESSION['models']='<h3>Имя модели:</h3>'.'</br>'.$namemodel[0]['modelcar'].'<h3>Комплектация:</h3>'.'</br>'.$namecomplect[0]['title'];
                    $_SESSION['complect'].='<h3>Идентификтор комплектации:</h3>'.$decription['idcomplectname'].'<h3>Технические характеристики комплектации:</h3>'.$decription['description'];


                }

            }
            

        }

    }






    public function actionMail()

    {
        $model = new Models();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $session = Yii::$app->session;
            $session->open();

            Yii::$app->mailer->compose()
                ->setFrom('zembuster1@yandex.ru')
                ->setTo($model->email)
                ->setSubject('Вы выбрали автомобиль:') // тема письма
                ->setTextBody('<h3>Отправитель:</h3>'.$model->email.$_SESSION['models'].'<h3>Виды комплектаций:</h3>'.$_SESSION['complect'])
                ->setHtmlBody('<h3>Отправитель:</h3>'.$model->email.$_SESSION['models'].'<h3>Виды комплектаций:</h3>'.$_SESSION['complect'])
                ->send();

            Yii::$app->session->setFlash('contactFormSubmitted');
            $session->close();
            return $this->redirect(['/site/cars']);
        }



    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionCars()
    {
        $content = file_get_contents('http://toyota-credit.360d.ru/cars/Models/all.json');
        if($content == '')
            $cars = array();
        else
            $cars = json_decode($content,true);


        $model = new Models();

        $connection = Yii::$app->db;

        $haracteristics=$connection->createCommand("SELECT description FROM idcomplect")->queryAll();
        $complectations=$connection->createCommand("SELECT * FROM complectations")->queryAll();
        $idcomplect=$connection->createCommand("SELECT * FROM idcomplect")->queryAll();
        $models_complect=$connection->createCommand("SELECT * FROM models_complects")->queryAll();
        foreach($cars as $key=>$cars2) {

            foreach($cars2 as $key2=>$cars3) {




                $model->id = $key;

                $model->modelcar = $key2;

                $idmodelcars=$connection->createCommand("SELECT id FROM models WHERE modelcar='".$key2."'")->queryAll();
                if(empty($idmodelcars)){
                    $model->insert();
                }

                foreach($cars2[$key2]['grades'] as $key3=>$cars4) {

                    if(empty($complectations) && empty($idcomplect)){


                    $connection->createCommand("INSERT INTO complectations (title) VALUES ('".$cars4['title']."') ON DUPLICATE KEY UPDATE title = '".$cars4['title']."' ")->execute();
                    $connection->createCommand("INSERT INTO idcomplect (idcomplectname) VALUES ('".$cars4['id']."') ON DUPLICATE KEY UPDATE idcomplectname = '".$cars4['id']."'")->execute();
                    }

                    foreach($cars[$key][$key2]['grades'][$key3]['technicalSpecification'] as $key4=>$cars5) {

                    $description[$cars4['id']][$cars5['type']][]=$cars5['title'].','.$cars5['details'];

                    }



                }




            }


        }


        $text='';
        if(empty($haracteristics)) {
            foreach ($description as $key1 => $descriptions) {

                foreach ($descriptions as $keys => $descriptions2) {

                    $descript = implode(",", $descriptions2);


                    $text .= '<h3>' . $keys.':' . '</h3>'."\r\n".$descript . '</br>'. "\r\n";


                }


                $connection->createCommand("Update idcomplect Set description ='" . $text . "' Where idcomplectname='" . $key1 . "'")->execute();


                $text = '';

            }
        }



        if(empty($models_complect)){

        $this->actionCarshasmany();



        }
        $models = Models::find()->all();
        return $this->render('cars',['models'=>$models]);
    }

    public function actionCarshasmany()
    {
       $content = file_get_contents('http://toyota-credit.360d.ru/cars/Models/all.json');
        if($content == '')
            $cars = array();
        else
           $cars = json_decode($content,true);


        $connection = Yii::$app->db;


        foreach($cars as $key=>$cars2) {

            foreach($cars2 as $key2=>$cars3) {

                $idmodelcars=$connection->createCommand("SELECT id FROM models WHERE modelcar='".$key2."'")->queryAll();

                foreach($cars2[$key2]['grades'] as $key3=>$cars4) {

                    $idcomplectcar=$connection->createCommand("SELECT id FROM complectations WHERE title='".$cars4['title']."'")->queryAll();

                    $idcomplectations=$connection->createCommand("SELECT id FROM idcomplect WHERE idcomplectname='".$cars4['id']."'")->queryAll();
                    if(isset($idmodelcars) && isset($idcomplectcar) && isset($idcomplectations)){
                        $connection->createCommand("INSERT INTO models_complects (idmodels,idcomplectations,idcomplect) VALUES ('".$idmodelcars[0]['id']."','".$idcomplectcar[0]['id']."','".$idcomplectations[0]['id']."') ")->execute();

                    }





                }




            }


        }

    }




























}
