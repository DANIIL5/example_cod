<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use app\models\Models;
use app\models\Complectations;
use yii\widgets\ActiveForm;

$this->title = 'Toyota';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <h3>Выберите модель автомобиля</h3>

    <?php
    foreach ($models as $model){

        $modelname[$model->id]=$model->modelcar;
       
    }?>

    <?php
    $form = ActiveForm::begin();
   
    $params = [
    'prompt' => 'Выберите модель...',
        'id' => 'modelcar'
    ];
    echo $form->field($model, 'model')->dropDownList($modelname,$params)->label('Модель');
    ActiveForm::end();
    ?>


    <?php
    $form = ActiveForm::begin();
    $modelname=[];
    $params = [
        'prompt' => 'Выберите комплектацию...',
        'id' => 'complectation'
    ];
    echo $form->field($model, 'complect')->dropDownList($modelname,$params)->label('Комплектация');
    ;
    ActiveForm::end();
    ?>
    <div class="tehnic">
        

    </div>

    <?php
    
    $form = ActiveForm::begin(['id' => 'send-form','action' => '/site/mail','options'=>['style' => 'display:none']
    ]); ?>


    <?= $form->field($model, 'email') ?>

    <?= Html::submitButton('Send', ['class' => 'btn btn-success']) ?>

    <?php ActiveForm::end() ?>


</div>
